-- SUMMARY --

The CookieCuttr module helps site owners to comply with the EU Directive on
 Privacy and Electronic Communications, known as the "Cookie law".
It presents site visitors with information and an opportunity to give or
 withhold consent to the use of cookies, by providing Drupal integration
 for the CookieCuttr jQuery plugin.

All the configuration options documented at http://cookiecuttr.com/#options
 can be controlled via the CMS.

For a full description of the module, visit the project page:
  http://drupal.org/project/cookiecuttr

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/cookiecuttr


-- REQUIREMENTS --

The plugin depends on jQuery and the jQuery cookie plugin from Drupal core.


-- INSTALLATION --

* Install as you would normally install a contributed Drupal module.
  See https://www.drupal.org/docs/extending-drupal/installing-drupal-modules


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:

  - Administer CookieCuttr

    This allows users to configure all options for CookieCuttr

* Configure options at
  Administration » Configuration » User Interface » CookieCuttr

  See http://cookiecuttr.com/#options for full documentation of the options.
